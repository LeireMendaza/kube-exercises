Preguntas:

1. kubectl logs -f --tail 10 nginx
2. kubectl get pod -o wide
3. kubectl exec -it nginx -- /bin/bash
4. Dentro del pod, ejecuto cd usr/share/nginx/html y cat index.html
5. kubectl get pod nginx --output=yaml

Adjunto un pdf con las imagenes de los comandos.
